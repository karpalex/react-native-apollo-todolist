const { DataSource } = require('apollo-datasource');


class TodosAPI extends DataSource {
  constructor({ store }) {
    super();
    this.store = store;
  }

  initialize(config) {
    this.context = config.context;
  }

  async addTodo({text}) {
    const res = await this.store.todo.create({text, status: false});
    return res ? this.todoReducer(res) : false;
  }

  async removeTodo({id}){

    const res = await this.store.todo.deleteOne({_id: id})
    return res.ok && res.deletedCount ? id : null
  }

  async updateTodo({id, status, text}) {
    const res = await this.store.todo.findOneAndUpdate({_id: id}, {status, text}, {new: true})
    console.log(res)
    return res ? this.todoReducer(res) : null;
  }

  async getTodos(){
   const res = await this.store.todo.find({})
    return Array.isArray(res)
      ? res.map(todo => this.todoReducer(todo)) : [];
  }

  todoReducer(todo) {
    return {
      id: todo._id || 0,
      text: todo.text,
      status: todo.status
    }
  }

}

module.exports = TodosAPI;
