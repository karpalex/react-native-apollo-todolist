module.exports = {
  Query: {
    todos: async (_, __, { dataSources }) => dataSources.todoAPI.getTodos(),
  },
  Mutation:{
    addTodo: async (_, {text}, {dataSources}) => dataSources.todoAPI.addTodo({text}),
    updateTodo: async (_, {id, status, text}, {dataSources}) => dataSources.todoAPI.updateTodo({id, status, text}),
    removeTodo: async (_, {id}, {dataSources}) => dataSources.todoAPI.removeTodo({id})
  }
};


