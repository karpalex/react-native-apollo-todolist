const { ApolloServer } = require('apollo-server');
const typeDefs = require('./schema');
const TodoAPI = require('./datasources/todo');
const { createStore } = require('./utils');

const resolvers = require('./resolvers');

const store = createStore();

const server = new ApolloServer({ 
  typeDefs,
  resolvers,
  dataSources: () => ({
    todoAPI: new TodoAPI({store}),
  })
});


server.listen().then(({ url }) => {
  console.log(`🚀 Server ready at ${url}`);
});