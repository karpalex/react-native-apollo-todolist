const { gql } = require('apollo-server');

const typeDefs = gql`

  type Query {
    todos: [Todo]
  }

  type Todo {
    id: ID!
    text: String
    status: Boolean
  }

  type Mutation {
    addTodo(text: String!): Todo
    updateTodo(id: ID!, status:Boolean, text:String): Todo
    removeTodo(id: ID!): ID
  }

`;

module.exports = typeDefs;
