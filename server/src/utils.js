const mongoose = require('mongoose');
const Schema = mongoose.Schema
//Set up default mongoose connection



module.exports.createStore = () => {
  const mongoDB = 'mongodb://127.0.0.1/todolist';
  mongoose.connect(mongoDB, { useNewUrlParser: true });
  
  mongoose.model('Todo', 
                new Schema({ text: String, status: Boolean, id: String}), 
                  'todos');    

  return { 'todo' : mongoose.model('Todo')};
};
