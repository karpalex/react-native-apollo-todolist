import React, {Component} from 'react';
import { createAppContainer, createDrawerNavigator} from 'react-navigation'
import {apiUrl as uri} from '../app.json';
import ApolloClient from 'apollo-boost';
import { ApolloProvider } from 'react-apollo';
import TodoScreen from './screens/TodoScreen'
import { InMemoryCache } from 'apollo-cache-inmemory'



const client = new ApolloClient({
  uri,
  cache: new InMemoryCache()
})

// just to try to implement navigator
const Navigator =  createDrawerNavigator({
    Todo: {
      screen: TodoScreen,
    },
})

const App = createAppContainer(Navigator);

export default class extends Component {
  render() {
    return (
      <ApolloProvider client={client}>
         <App/>
      </ApolloProvider>
    );
  }
}

