import {Text, TextInput, View, CheckBox} from 'react-native';

import React from 'react';
import styles from './styles'
import { useState } from 'react'

export default ( {updateTodoHandler, removeTodoHandler, status, text, id}) => {

    [value, setValue] = useState(text)

    const updateTodo = (id, status, event) => {
        event.persist()
        updateTodoHandler(id, status, value)
    }

    const changeTextHandler = (text) => {
        setValue(text)
    }

    return (
        <View style ={styles.container}> 
            <CheckBox
                value={status}
                onChange = {(event) => {
                    updateTodo(id, !status, event)                
                }}
            /> 

            <TextInput 
                onChangeText = {changeTextHandler}
                onBlur = {(event) => {
                    updateTodo(id, status,  event)            
                }}
                style = {status ? {...styles.todoDone, ...styles.text }: styles.text}>
                {text}
            </TextInput>

            <Text 
                style={styles.remove}
                onPress= {(event)=> {
                    event.persist()
                    removeTodoHandler(id)     
                }}
            
            >x</Text>

        </View>
    )
}