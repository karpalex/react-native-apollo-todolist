import {StyleSheet} from 'react-native'


const styles = StyleSheet.create({
    container: {
          backgroundColor: 'white',
          padding: 20,
          borderRadius: 4,
          borderWidth: 0.5,
          display: 'flex',
          flexDirection: 'row',
          justifyContent: 'space-between',
          alignItems: 'center',
          borderColor: '#eee',
          margin: 5,
          position: 'relative',
  
          shadowColor: '#000',
          shadowOffset: {
              width: 0,
              height: 4,
          },
          shadowOpacity: 0.3,
          shadowRadius: 4.65,
  
         
    },
    todoDone :{
        textDecorationLine: 'line-through',
    },

    text : {
        width: '80%',
        
    },

    remove: {
        color: '#800000',
        fontSize: 25
      
    }
   
});


export default styles