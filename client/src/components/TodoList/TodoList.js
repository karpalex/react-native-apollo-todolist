import React from 'react';
import {View} from 'react-native';
import Todo from '../Todo/Todo'


const TodoList =  ({updateTodoHandler,removeTodoHandler, todos}) => 
   
    <View>
        {todos.map(todo => (
            <Todo 
                key={todo.id} 
                id={todo.id} 
                text={todo.text}
                status={todo.status}
                updateTodoHandler ={updateTodoHandler}
                removeTodoHandler ={removeTodoHandler}
            />
        ))}
    </View>
  
export default TodoList
