import {StyleSheet} from 'react-native'

const styles = StyleSheet.create({
    input: {
        height: 40, 
        borderColor: 'gray', 
        borderWidth: 1,
        
        borderRadius: 4,
        borderWidth: 0.5,
        flex: 0.9
    },

    container: {
        padding: 10,
        display: 'flex', 
        justifyContent: 'center',
        flexDirection: 'row',
    
    },

    button: {
        margin: 10,
        color: "#841584",
        flex: 0.1,
        padding: 10
        
    }
   
})

export default styles
