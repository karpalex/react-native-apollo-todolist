import React from 'react';
import { TextInput, Button, View } from 'react-native'
import styles from './styles'

const AddTodoInput = ({addTodoHandler, setValue, value}) => {

    const changeTextHandler = (value) => {
        setValue(value)
    }

    return (
        <View style = {styles.container}>
            <TextInput style ={styles.input}     
                editable = {true}
                value = {value}
                onChangeText = {changeTextHandler}
            />
            <Button style = {styles.Button}
                title = 'Add'
                onPress = {(evt) =>{
                    evt.persist()
                    addTodoHandler(value)
                }}
            />
        </View>
       
    )
}



export default AddTodoInput