import gql from 'graphql-tag';


const GET_TODOS = gql`
    query getTodos {
        todos {
            id
            text
            status
        }
    }
`

export  {
    GET_TODOS
}