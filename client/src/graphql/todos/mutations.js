import gql from 'graphql-tag';

const ADD_TODO = gql`
  mutation AddTodo($text: String!) {
    addTodo(text: $text) {
      id
      status
      text
    }
  }
`

const UPDATE_TODO = gql`
  mutation UpdateTodo($id: ID!, $text: String!, $status: Boolean!){
    updateTodo(id: $id, text: $text, status: $status){
      id
      status
      text
    }
  }
`

const REMOVE_TODO = gql`
  mutation RemoveTodo($id: ID!){
    removeTodo(id: $id)
  }
`


export {
    ADD_TODO,
    UPDATE_TODO,
    REMOVE_TODO
}