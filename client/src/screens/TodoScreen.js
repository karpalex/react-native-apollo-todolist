import React from 'react';
import { useState } from 'react'
import { ScrollView, Text } from 'react-native'
import TodoList from '../components/TodoList/TodoList'

import AddTodoInput from '../components/AddTodoInput/AddTodoInput'

import { GET_TODOS } from '../graphql/todos/queries'
import { ADD_TODO, UPDATE_TODO, REMOVE_TODO } from '../graphql/todos/mutations'


const TodoScreen = ({todoQuery, updateTodo, removeTodo, addTodo }) => {

    const [value, setValue] = useState('')

    const updateTodoHandler =  (id, status, text) => updateTodo({variables:  {id, status, text}})
    

    const removeTodoHandler = (id) =>  {
       removeTodo({
            variables: {id},
            update: (store,  { data: { removeTodo }}) => {
                const data = store.readQuery({query: GET_TODOS });
                
                const removedIndex = data.todos.map(todo => {
                    return todo.id;
                }).indexOf(removeTodo);
                data.todos.splice(removedIndex, 1);

                store.writeQuery({ query: GET_TODOS, data });
            }
        })
    }

    const addTodoHandler =  (text) => {
        addTodo({
            variables:  {text},
            update: (store, { data: { addTodo } }) => {
                const data = store.readQuery({query: GET_TODOS });
                data.todos.unshift(addTodo);
                store.writeQuery({ query: GET_TODOS, data });
            },
        }).then(()=>{
            setValue('')
        })

    }



    const todoList = !todoQuery.loading ? (
        <TodoList
                todos = {todoQuery.todos}
                updateTodoHandler = {updateTodoHandler}
                removeTodoHandler = {removeTodoHandler}
        />
    ): <Text>loading</Text> // TODO: add spinner 

    return (
        <ScrollView>
            <AddTodoInput
                value = {value}
                addTodoHandler = {addTodoHandler}
                setValue = {setValue}
            />
            {todoList}
        </ScrollView>
    )
}

export default compose(
    graphql(GET_TODOS, {variables: {status: false}, name: 'todoQuery'}),
    graphql(ADD_TODO, {name: 'addTodo'}),
    graphql(UPDATE_TODO, {name: 'updateTodo'}),
    graphql(REMOVE_TODO, {name: 'removeTodo'})
)(TodoScreen)

